package com.devcamp.j62crudfrontend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.devcamp.j62crudfrontend.model.CVoucher;
import com.devcamp.j62crudfrontend.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;

    ////////////////////// GET //////////////////////////////
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }

    public ResponseEntity<CVoucher> getVoucherById(long id) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
            return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

      //////////////////////////// POST SERVICES///////////////////////////
      public ResponseEntity<Object> createVoucher(CVoucher voucher) {
        try {

            Optional<CVoucher> voucherData = pVoucherRepository.findById(voucher.getId());
            if (voucherData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
            }

            
            CVoucher newVoucher = pVoucherRepository.save(voucher);
            return new ResponseEntity<>(newVoucher, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

     //////////////////////////// PUT SERVICES///////////////////////////
     public ResponseEntity<Object> updateVoucherById(long id, CVoucher pVoucher) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        if (voucherData.isPresent()) {
            CVoucher voucherUpdate = voucherData.get();
           
            voucherUpdate.setMaVoucher(pVoucher.getMaVoucher());
			voucherUpdate.setPhanTramGiamGia(pVoucher.getPhanTramGiamGia());
			voucherUpdate.setGhiChu(pVoucher.getGhiChu());
			voucherUpdate.setNgayCapNhat(new Date());
            

            try {
                return new ResponseEntity<>(pVoucherRepository.save(voucherUpdate), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
        }
    }
    
     //////////////////////////// DELETE SERVICES///////////////////////////
     public ResponseEntity<Object> deleteVoucherById(long id) {
        try {
            pVoucherRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteVoucher() {
        try {
            pVoucherRepository.deleteAll();
            ;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
