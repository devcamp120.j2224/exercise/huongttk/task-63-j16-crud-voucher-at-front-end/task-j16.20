package com.devcamp.j62crudfrontend.controller;

import java.util.*;

import com.devcamp.j62crudfrontend.model.CVoucher;
import com.devcamp.j62crudfrontend.service.CVoucherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/vouchers")
@CrossOrigin(value = "*", maxAge = -1)
public class CVoucherController {
    @Autowired
    CVoucherService pVoucherService;
    // Lấy danh sách voucher CÓ dùng service.
   @GetMapping
    public ResponseEntity<List<CVoucher>> getAllVouchersByService() {
        try {
            return new ResponseEntity<>(pVoucherService.getVoucherList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }

	 // Lấy voucher theo {id} dùng service
	 @GetMapping("/{id}") // Dùng phương thức GET
	 public ResponseEntity<CVoucher> getVoucherById(@PathVariable("id") long id) {
		 ResponseEntity<CVoucher> response = pVoucherService.getVoucherById(id);
		return response;
	 }

	 @PostMapping // Dùng phương thức POST
	 public ResponseEntity<Object> createVoucher(@RequestBody CVoucher voucher) {
		 try {
			 return new ResponseEntity<>(pVoucherService.createVoucher(voucher), HttpStatus.OK);
		 } catch (Exception e) {
			 return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	 }

	 @PutMapping("/{id}") // Dùng phương thức POST
	 public ResponseEntity<Object> updateVoucherById(@PathVariable("id") long id, @RequestBody CVoucher voucher) {
 
		 try {
			 return new ResponseEntity<Object>(pVoucherService.updateVoucherById(id, voucher), HttpStatus.OK);
		 } catch (Exception e) {
			 return ResponseEntity.unprocessableEntity()
					 .body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
		 }
 
	 }
 
	 @DeleteMapping // Dùng phương thức DELETE
	 public  ResponseEntity<Object> deleteAllVoucher() {
		
		 ResponseEntity<Object> voucher= pVoucherService.deleteVoucher();
		 return voucher;
	 }
 
	 @DeleteMapping("/{id}") // Dùng phương thức DELETE
	 public ResponseEntity<Object> deleteVoucherById(@PathVariable("id") long id) {
			ResponseEntity<Object> response = pVoucherService.deleteVoucherById(id);
			return response;
		
	 }
	 // Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
	 // @DeleteMapping("/vouchers")// Dùng phương thức DELETE
	 // public ResponseEntity<CVoucher> deleteAllCVoucher() {
	 // try {
	 // pVoucherRepository.deleteAll();
	 // return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	 // } catch (Exception e) {
	 // System.out.println(e);
	 // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	 // }
	 // }

 


    /*// Lấy voucher theo {id} Ko dùng service
    @GetMapping("/vouchers/{id}")// Dùng phương thức GET
	public ResponseEntity<CVoucher> getCVoucherByIdWithService(@PathVariable("id") long id) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
    // Tạo MỚI voucher KO dùng service sử dụng phương thức POST
    @PostMapping("/vouchers")// Dùng phương thức POST
	public ResponseEntity<Object> createCVoucherWithService(@RequestBody CVoucher pVouchers) {
		try {			
			Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
			if(voucherData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
			}
			pVouchers.setNgayTao(new Date());
			pVouchers.setNgayCapNhat(null);
			CVoucher _vouchers = pVoucherRepository.save(pVouchers);
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			//return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
		}
	}
    // Sửa/update voucher theo {id} KO dùng service, sử dụng phương thức PUT
    @PutMapping("/vouchers/{id}")// Dùng phương thức PUT
	public ResponseEntity<Object> updateCVoucherByIdWithService(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			CVoucher voucher= voucherData.get();
			voucher.setMaVoucher(pVouchers.getMaVoucher());
			voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
			voucher.setGhiChu(pVouchers.getGhiChu());
			voucher.setNgayCapNhat(new Date());
			try {
				return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Voucher:"+e.getCause().getCause().getMessage());
			}
		} else {
			return ResponseEntity.badRequest().body("Failed to get specified Voucher: "+id + "  for update.");
		}
	}
    // Xoá/delete voucher theo {id} KO dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/vouchers/{id}")// Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteCVoucherByIdWithService(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    // Xoá/delete tất cả voucher KO dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/vouchers")// Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteAllCVoucherWithService() {
		try {
			pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}*/
}
